# Advent of Code 2019
This project contains solutoins for [Advent of Code 2019](http://adventofcode.com/2019). I encourage you to try to solve problems alone before looking at my imperfect code.

## Dependencies
- C++ compiler supporting C++17.
- [GNU make](https://www.gnu.org/software/make/)

## Currently solved problems
- Day 01
- Day 02
