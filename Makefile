all:
	@for DIR in */; \
	do \
	    $(MAKE) -C "$$DIR" all; \
	done;

debug:
	@for DIR in */; \
	do \
	    $(MAKE) -C "$$DIR" debug; \
	done;

run:
	@for DIR in */; \
	do \
	    $(MAKE) -C "$$DIR" run; \
	done;

clean:
	@for DIR in */; \
	do \
	    $(MAKE) -C "$$DIR" clean; \
	done;

dist-clean:
	@for DIR in */; \
	do \
	    $(MAKE) -C "$$DIR" dist-clean; \
	done;

.PHONY: all clean dist-clean
