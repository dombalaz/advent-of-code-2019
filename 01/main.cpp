#include <iostream>
#include <fstream>
#include <cstdlib>

int main(int argc, char **argv)
{
    if(argc != 2) {
        std::cerr << "File was not entered." << std::endl;
        return EXIT_FAILURE;
    }
    std::ifstream f(argv[1], std::ifstream::in);
    if(!f.is_open()) {
        std::cerr << "File failed to open." << std::endl;
        return EXIT_FAILURE;
    }
    std::string s;
    std::int64_t result1 = 0;
    std::int64_t result2 = 0;
    while(std::getline(f, s)) {
        std::int64_t n = std::stol(s);
        std::int64_t fuel = (n / 3) - 2;
        result1 += fuel;
        while(fuel > 0) {
            result2 += fuel;
            fuel = (fuel / 3) - 2;
        }
    }
    std::cout << "Advent of Code 2019 - Day 01" << std::endl;
    std::cout << "Part One: " << result1 << std::endl;
    std::cout << "Part Two: " << result2 << std::endl;
}
