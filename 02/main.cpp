#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

void execute_program(std::vector<std::uint64_t> &vec)
{
    for(size_t i = 0; i < vec.size(); i += 4) {
        if(vec.at(i) != 1 && vec.at(i) != 2) {
            break;
        }
        if(i + 4 > vec.size()) {
            break;
        }
        if(vec.at(i + 3) >= vec.size()) {
            break;
        }
        vec[vec.at(i + 3)] = vec.at(i) == 1 
            ? vec.at(vec.at(i + 1)) + vec.at(vec.at(i + 2))
            : vec.at(vec.at(i + 1)) * vec.at(vec.at(i + 2));
    }
}

int main(int argc, char **argv)
{
    if(argc != 2) {
        std::cerr << "File was not entered." << std::endl;
        return EXIT_FAILURE;
    }
    std::ifstream f(argv[1], std::ifstream::in);
    if(!f.is_open()) {
        std::cerr << "File failed to open." << std::endl;
        return EXIT_FAILURE;
    }
    std::string s;
    std::vector<std::uint64_t> vec1;
    std::getline(f, s);
    size_t pos = 0;
    while((pos = s.find(',')) != std::string::npos) {
        std::string token = s.substr(0, pos);
        s.erase(0, pos + 1);
        vec1.push_back(std::stoul(token));
    }
    vec1.push_back(std::stoul(s));
    const std::vector<std::uint64_t> orig = vec1;
    vec1[1] = 12;
    vec1[2] = 2;
    execute_program(vec1);

    const std::uint64_t searched = 19690720;
    std::uint64_t res = 0;
    for(std::uint64_t i = 0; i < 100; ++i) {
        for(std::uint64_t j = 0; j < 100; ++j) {
            std::vector<std::uint64_t> vec = orig;
            vec[1] = i;
            vec[2] = j;
            execute_program(vec);
            if(vec.at(0) == searched) {
                res = 100 * i + j;
                goto out;
            }
        }
    }
out:
    std::cout << "Advent of Code 2019 - Day 02" << std::endl;
    std::cout << "Task 1: " << vec1.at(0) << std::endl;
    std::cout << "Task 2: " << res << std::endl;
    return EXIT_SUCCESS;
}
